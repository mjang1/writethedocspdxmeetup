Our basic understanding for Meetups for 2020. Basic theme: simplify!

Organizers will rotate responsibility for setting up meetups, in relatively quiet locations close to the person whose taking responsibility.

We're simplifying by not worrying about an "official" office-type location -- and will do most of our meetups in some public place like a restaurant or coffee shop.

Example: Mo's taking January. He'll plan to do something during the first full week of that month, before the deadline for the WtD NA call for proposals. He'll pick the place (probably a friendly relatively quiet restaurant) a month before and let us know. Barring something really objectionable, he'll post the place on Meetup.com at least 3 weeks before. We told him to pick someplace relatively quiet, convenient for him. (Mo's using the SoundCheck app to help find relatively quiet places.)

Kristen's taking February. She'll pick a place convenient for her, same criteria.

After that, the plan is to have other organizers (Amy Qualls, Christy Lutz, Erin Grace, Mike Jang) join the rotation.

We may have months where we'll combine forces with another meetup and/or do things like the supermeetup we had with Sarah Read of PSU back in October.

That way, we'll minimize the load on each of us, and have meetups announced in time for people to RSVP.

In this README, we have a basic directory structure; example, we have a space for our January 2020 meetup in the meetups/Jan2020 subdirectory.
